from .outputHelp import outputHelpGeneral

def outputHelp(detailed=False):
    outputHelpGeneral("Name\nDescription", "\nUsage:\nPython file: funcManagers/functions/file.py\nManager file: funcManagers/fileM.py", detailed)

def checkArgs(argv, argsNo):
    ## Check args
    ## if enough args and arg != "-h" or "--help":
    ##     from funcManager.functions import file and fileAbbreviation
    ##     Run command
