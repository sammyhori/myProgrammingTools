from .outputHelp import outputHelpGeneral

def outputHelp(detailed=False):
    outputHelpGeneral("ct: Command timer\nPrints the time taken for a command to run.", "\nUsage: mpt ct [command or 'command && command' for mutiple commands]\nPython file: funcManagers/functions/commandTimer.py\nManager file: funcManagers/commandTimerM.py", detailed)

def checkArgs(argv, argsNo):
    if argsNo == 2:
        print("'mpt ct' requires a command to work. See 'mpt ct --help'.")
    elif argv[2] == "-h" or argv[2] == "--help":
        print("mpt ct HELP")
        outputHelp(True)
    else:
        from .functions import commandTimer as ct
        ct.main(' '.join(argv[2:]))
