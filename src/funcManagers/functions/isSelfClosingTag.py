def isSCT(tagname):
    if tagname in ("area", "base", "br", "col", "embed", "hr", "img", "link", "meta", "param", "source", "track", "wbr"):
        return True
    else:
        return False

def main(tagname):
    tagname = tagname.strip('<> \t')
    if isSCT(tagname):
        print(f"'{tagname}' is a self closing tag")
    else:
        print(f"'{tagname}' is not a self closing tag")
