from .outputHelp import outputHelpGeneral

def outputHelp(detailed=False):
    outputHelpGeneral("isct: Is self closing tag\nTells you if entered tag is self closing or not.", "\nUsage: mpt isct [tagname]\nPython file: funcManagers/functions/isSelfClosingTag.py\nManager file: funcManagers/isSelfClosingTag.py", detailed)

def checkArgs(argv, argsNo):
    if argsNo == 2:
        print("'mpt isct' requires one argument. See 'mpt isct --help'")
    else:
        arg2 = argv[2]
        if arg2 == "-h" or arg2 == "--help":
            print("mpt isct HELP")
            outputHelp(True)
        else:
            if argsNo > 3:
                print("Too many arguments for 'mpt isct'. Using first one.")
            from .functions import isSelfClosingTag as isct
            isct.main(arg2)
